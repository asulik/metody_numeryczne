# Importuję niezbędne biblioteki
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

# Zdefinuję podstawowe funkcje:
def fdx(x, y):
    return 10*(y-x)

def fdy(x, y, z):
    return x*(28-z)-y

def fdz(x,y,z):
    return x*y-8/3*z

def SolveAndDraw(t_max, t_n):
# Wyznaczę wektor czasu
    t_step = t_max/t_n
    t_arr = np.arange(0, t_max+t_step, t_step)

    # Ustalam warunki początkowe
    x = np.array([1])
    y = np.array([1])
    z = np.array([1])

    for t in t_arr:
        #Liczę K1:
        K1x = fdx(x[-1], y[-1])
        K1y = fdy(x[-1], y[-1], z[-1])
        K1z = fdz(x[-1], y[-1], z[-1])

        #Liczę K2:
        K2x = fdx(x[-1]+1/2*K1x*t_step,
                  y[-1]+1/2*K1y*t_step)

        K2y = fdy(x[-1]+1/2*K1x*t_step,
                  y[-1]+1/2*K1y*t_step,
                  z[-1]+1/2*K1z*t_step)

        K2z = fdz(x[-1]+1/2*K1x*t_step,
                  y[-1]+1/2*K1y*t_step,
                  z[-1]+1/2*K1z*t_step)

        #Liczę K3
        K3x= fdx(x[-1]+1/2*K2x*t_step,
                 y[-1]+1/2*K2y*t_step)

        K3y = fdy(x[-1]+1/2*K2x*t_step,
                  y[-1]+1/2*K2y*t_step,
                  z[-1]+1/2*K2z*t_step)

        K3z = fdz(x[-1]+1/2*K2x*t_step,
                  y[-1]+1/2*K2y*t_step,
                  z[-1]+1/2*K2z*t_step)

        # Liczę K4
        K4x = fdx(x[-1]+1/2*K3x*t_step,
                  y[-1]+1/2*K3y*t_step)

        K4y = fdy(x[-1]+1/2*K3x*t_step,
                  y[-1]+1/2*K3y*t_step,
                  z[-1]+1/2*K3z*t_step)

        K4z = fdz(x[-1]+1/2*K3x*t_step,
                  y[-1]+1/2*K3y*t_step,
                  z[-1]+1/2*K3z*t_step)

        # Wyznaczę wartości składowych a następnie dodam je do wektorów je przechowujących zgodnie
        # z klasyczną metodą RK IV rzędu (por wykład prof. Piotra Fronczaka)
        x_val = x[-1] + 1/6*(K1x + 2*K2x + 2*K3x + K4x)*t_step
        y_val = y[-1] + 1/6*(K1y + 2*K2y + 2*K3y + K4y)*t_step
        z_val = z[-1] + 1/6*(K1z + 2*K2z + 2*K3z + K4z)*t_step

        x = np.append(x, x_val)
        y = np.append(y, y_val)
        z = np.append(z, z_val)

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    ax.set_title("Lorenc dla tmax = "+str(t_max))
    ax.plot3D(x,y,z)

fig = plt.figure(figsize=(20,10))
ax = fig.add_subplot(1,2,1, projection='3d')
SolveAndDraw(t_max=20, t_n = 500)
ax = fig.add_subplot(1,2,2, projection='3d')
SolveAndDraw(t_max=100, t_n = 10000)

plt.show()
