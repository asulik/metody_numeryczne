# Numerical Methods Course Tasks Repository

Welcome to the Numerical Methods Course Tasks Repository!

Within this git repository, you'll find my solutions to a series of tasks assigned during a numerical methods course. These tasks span a diverse range of topics, including:

1. **Numerical Solutions of Differential Equations:**
   Explore my implementations of algorithms designed to numerically solve differential equations, gaining insights into approximation techniques for various scenarios.

2. **Polynomial Algorithms:**
   Delve into my solutions for tasks related to polynomial algorithms. Witness the practical application of algorithms in solving polynomial equations, providing a comprehensive understanding of numerical solutions.

3. **Optimization Algorithms:**
   Discover my approaches to optimization problems through tasks that involve the implementation of algorithms. Gain practical experience in finding optimal solutions using various optimization techniques.

4. **Algebraic Calculations Algorithms:**
   Explore my solutions for tasks focusing on algebraic calculations. Witness the application of algorithms designed for algebraic problem-solving, covering a spectrum of mathematical scenarios and operations.

Please note that the text in these Jupyter notebooks is written in Polish, as the course was conducted in Polish.

Feel free to review and utilize these solutions as a valuable resource for your numerical methods studies. Whether you're seeking insights or inspiration, these implementations provide practical examples of tackling numerical challenges. Happy coding!
